package hu.bme.aut.musicartistsapp.model

import retrofit2.http.Url

data class ArtistSearchModel(
    val name: String,
    val mbid: String,
    val url: Url,
    val small_image: Url,
    val image: Url,
    val streamable: Int
)