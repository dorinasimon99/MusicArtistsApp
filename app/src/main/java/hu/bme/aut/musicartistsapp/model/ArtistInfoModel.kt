package hu.bme.aut.musicartistsapp.model

import retrofit2.http.Url

data class ArtistInfoModel(
    val name: String,
    val listeners: Int,
    val mbid: String,
    val url: Url,
    val streamable: Int,
    val image: List<ImageModel>
)