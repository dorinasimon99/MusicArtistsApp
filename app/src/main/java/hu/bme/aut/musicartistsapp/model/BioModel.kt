package hu.bme.aut.musicartistsapp.model

import com.google.gson.annotations.SerializedName
import java.util.*

data class BioModel(
    @SerializedName("published")
    val published: Date,
    @SerializedName("summary")
    val summary: String,
    @SerializedName("content")
    val content: String
)
