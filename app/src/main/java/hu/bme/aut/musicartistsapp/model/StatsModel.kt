package hu.bme.aut.musicartistsapp.model

import com.google.gson.annotations.SerializedName

data class StatsModel(
    @SerializedName("listeners")
    val listeners: Int,
    @SerializedName("playcount")
    val plays: Int
)
