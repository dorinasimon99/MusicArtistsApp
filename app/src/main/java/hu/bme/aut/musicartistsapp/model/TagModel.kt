package hu.bme.aut.musicartistsapp.model

import com.google.gson.annotations.SerializedName
import retrofit2.http.Url

data class TagModel(
    @SerializedName("name")
    val name: String,
    @SerializedName("url")
    val url: Url
)
