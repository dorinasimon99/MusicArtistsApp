package hu.bme.aut.musicartistsapp

import android.graphics.Movie
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import hu.bme.aut.musicartistsapp.adapter.ArtistsAdapter
import hu.bme.aut.musicartistsapp.api.LastFMApiInterface
import hu.bme.aut.musicartistsapp.model.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    private lateinit var apiInterface : LastFMApiInterface
    private lateinit var artistsAdapter : ArtistsAdapter
    private lateinit var recyclerView : RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recyclerView = findViewById(R.id.artistsRecyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)

        apiInterface = LastFMApiInterface.create()

        artistsAdapter = ArtistsAdapter(this, listOf())

        recyclerView.adapter = artistsAdapter

        val searchView = findViewById<SearchView>(R.id.searchView)
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                search(query!!)
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return false
            }
        })
    }

    fun search(artistName: String) {
        val results = apiInterface.getArtistSearch("artist.search", artistName)

        results.enqueue( object : Callback<Results> {
            override fun onResponse(call: Call<Results>?, response: Response<Results>?) {

                if(response?.body() != null) {
                    recyclerView.apply {
                        layoutManager = LinearLayoutManager(this@MainActivity)
                        adapter = ArtistsAdapter(this@MainActivity, response.body()!!.results.matches.artist)
                    }
                }
            }

            override fun onFailure(call: Call<Results>, t: Throwable) {
                Log.e("MainActivity", "Failure: ${t.message}")
            }
        })
    }
}