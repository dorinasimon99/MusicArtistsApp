package hu.bme.aut.musicartistsapp.model

import com.google.gson.annotations.SerializedName

data class ArtistList (
    @SerializedName("artist")
    val artist: ArtistInfoModel?
)

data class Results(
    @SerializedName("results")
    val results: ArtistsMatches
)

data class ArtistsMatches(
    @SerializedName("artistmatches")
    val matches : SearchList
)

data class SearchList(
    @SerializedName("artist")
    val artist: List<ArtistInfoModel>

)