package hu.bme.aut.musicartistsapp.api

import hu.bme.aut.musicartistsapp.model.*
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface LastFMApiInterface {

    @GET("/2.0/")
    fun getArtistSearch(@Query("method") method: String = "artist.search",
                      @Query("artist") artistName: String,
                      @Query("api_key") apiKey: String = API_key,
                      @Query("format") format: String = "json") : Call<Results>

    @GET("/2.0/")
    fun getArtistInfo(@Query("method") method: String = "artist.getinfo",
                      @Query("artist") artistName: String,
                      @Query("api_key") apiKey: String = API_key,
                      @Query("format") format: String = "json") : Call<ArtistList>

    companion object {
        val base_URL = "http://ws.audioscrobbler.com"
        val API_key = "b0500349e3e60adffb4d16408e8f62fe"

        fun create() : LastFMApiInterface {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(base_URL)
                .build()
            return retrofit.create(LastFMApiInterface::class.java)
        }
    }
}
