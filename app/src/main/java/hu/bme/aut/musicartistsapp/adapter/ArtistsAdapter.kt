package hu.bme.aut.musicartistsapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import hu.bme.aut.musicartistsapp.R
import hu.bme.aut.musicartistsapp.model.ArtistInfoModel

class ArtistsAdapter(val context: Context, val artistList : List<ArtistInfoModel>?) : RecyclerView.Adapter<ArtistsAdapter.ArtistsViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ArtistsAdapter.ArtistsViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.single_artist, parent, false)
        return ArtistsViewHolder(view)
    }

    override fun onBindViewHolder(holder: ArtistsAdapter.ArtistsViewHolder, position: Int) {
        Glide.with(context).load(artistList!!.get(position).image[0].text)
            .apply(RequestOptions().centerCrop())
            .into(holder.artistImage)
        holder.artistName.text = artistList.get(position).name
        holder.listeners.text = artistList.get(position).listeners.toString()
    }

    override fun getItemCount(): Int {
        if (artistList != null) {
            return artistList.size
        }
        return 0
    }

    inner class ArtistsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val artistImage: ImageView = itemView.findViewById(R.id.artistImage)
        val artistName: TextView = itemView.findViewById(R.id.artistName)
        val listeners: TextView = itemView.findViewById(R.id.listeners)
    }
}