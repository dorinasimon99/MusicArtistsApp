package hu.bme.aut.musicartistsapp.model

import com.google.gson.annotations.SerializedName
import retrofit2.http.Url

data class ImageModel(
    @SerializedName("#text")
    val text: Url,
    @SerializedName("size")
    val size: String
)
